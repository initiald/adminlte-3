<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/master', function () {
    return view('master');
});
Route::get('/starter', function () {
    return view('starter');
});
Route::get('/test', function () {
    $menu = [];
    foreach (config('adminlte.menu') as $key => $value) {
        $menu[] = $value;
    }
    // return $menu[0]['type'];
    return config('adminlte.menu')[0];
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// group
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // Route::get('/', function () {
    //     return view('welcome');
    // });

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
